# Tecnologias utilizadas na Comp Júnior

Página que descreve todas as tecnologias utilizadas pela Comp Júnior

1. [Front-end](#tecnologias-para-desenvolvimento-front-end)
  1. [HTML5](#html5)
  2. [CSS3](https://developer.mozilla.org/en/docs/Web/CSS/CSS3)
  3. [JavaScript](https://developer.mozilla.org/en-US/docs/Web/JavaScript)
  4. Frameworks
    1. [AngularJS](http://angularjs.org/)
    2. [Bootstrap](http://getbootstrap.com/)
2. [Back-end](#tecnologias-para-desenvolvimento-back-end)

## Tecnologias para desenvolvimento Front-end
Nessa seção, são descritas as tecnologias utilizadas para a construção de telas utilizando ferramentas de desenvolvimento front-end.

### Linguagens

#### HTML5
Descrição sobre o que é a linguagem HTML5. Links de apoio.

#### CSS3
Descrição sobre o que é o CSS3

#### JavaScript
Descrição sobre o que é o Javascript

### Frameworks

#### Bootstrap
Framework desenvolvido pelo Twitter. Explicação sobre o Bootstrap

#### AngularJS
Framework desenvolvido pela Google. Explicação sobre o AngularJS.

### Ferramentas

#### SASS
Compilador que gera CSS.

#### Jade
Compilador que gera HTML

#### Grunt
Automatizador de tarefas feito em JavaScript

## Tecnologias para desenvolvimento Back-end

### Linguagens

#### PHP
Descrição sobre o PHP

#### SQL
Descrição sobre o SQL

### Frameworks

#### CakePHP 3.x
Descrição sobre o CakePHP 3.x

### Ferramentas

#### MySql/MariaDB
Explicação sobre o banco de dados MySQL/MariaDB
